# datetime-string

[![Build Status](https://gitlab.com/nop_thread/datetime-string/badges/develop/pipeline.svg)](https://gitlab.com/nop_thread/datetime-string/pipelines/)
[![Latest version](https://img.shields.io/crates/v/datetime-string.svg)](https://crates.io/crates/datetime-string)
[![Documentation](https://docs.rs/datetime-string/badge.svg)](https://docs.rs/datetime-string)
![Minimum supported rustc version: 1.47](https://img.shields.io/badge/rustc-1.47+-lightgray.svg)

Datetime string types.

## MSRV

MSRV (Minimum Supported Rust Version) is 1.47.0.

However, if `time03` feature is enabled, newer version of rustc is required.
`time` crate v0.3 bumps MSRV in semver-incompatible way, so fixed MSRV cannot
be guaranteed in this case.

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE.txt](LICENSE-APACHE.txt) or
  <https://www.apache.org/licenses/LICENSE-2.0>)
* MIT license ([LICENSE-MIT.txt](LICENSE-MIT.txt) or
  <https://opensource.org/licenses/MIT>)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
